# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 11:36:07 2015

@author: laptop
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

plt.close("all")

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443 *np.sin(2*x*0.6067)
    return y
def add_noise(y):
 np.random.seed(14)
 varNoise = np.max(y) - np.min(y)
 y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
 return y_noisy
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)
plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]
x = x[:, np.newaxis] 
y_measured = y_measured[:, np.newaxis]
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]
plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)
linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)
print ('Model je oblika y_hat = Theta0 + Theta1 * x')
print ('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)
x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

a=np.ones([70,1])
a1=np.ones([29,1])

xt2=np.column_stack((a,xtrain))
#inv(xt*x) * x transpoze * y
teta=np.dot(np.dot(np.linalg.inv(np.dot(xt2.transpose(),xt2)),xt2.transpose()),ytrain)
print ('y_h_pred = ', teta[0], '+', teta[1], '*x')

theta=np.array([[0],[1]])
theta1=np.ones([2,1])
#theta=theta.transpose()

alfa=0.01;



#theta[0]=theta[0]-alfa*grad[0]
#theta[1]=theta[1]-alfa*grad[1]
plt.figure(4)
for i in range(0,10000):
    
    h=np.dot(xt2,theta);    
    loss=h-ytrain;
    grad=np.dot(xt2.T,loss)/len(xt2)

    #temp=np.zeros([2,1])
    #theta[0]=theta[0]-alfa*(1/len(xt2)*np.dot(xt2[:,0],loss))
    #theta[1]=theta[1]-alfa*(1/len(xt2)*np.dot(xt2[:,1],loss))
#    theta[0]=theta[0]-alfa*grad[0]
#    theta[1]=theta[1]-alfa*grad[1]
    theta=theta-alfa*grad
    plt.plot(i,theta.T,'+')
 
plt.figure(5)
for j in range(1,len(loss)):
    plt.bar(j,loss[j])
#plt.plot(h,'+')
#plt.plot(ytrain,'o')
xtest2=np.column_stack((a1,xtest))
#b=1/len(loss)*sum(loss**2)  
#yc=1/len(ytrain)*sum(ytrain)  
#R=1-(sum(loss**2)/sum(ytrain-yc))

def sred_2(x,y,h):
    b=1/len(x)*sum((y-np.dot(x,h))**2) 
    return b
    
def koef_det(x,y,h):
    yc=1/len(y)*sum(y)  
    b=sum((y-np.dot(x,h))**2)
    a=sum(y-yc)
    return(1-b/a)
    
    
sred_2(xt2,ytrain,theta)    
print(koef_det(xt2,ytrain,theta))
print(r2_score(ytrain,h))
